#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri May 18 00:42:24 2018

@author: yoobin
"""
# Submit

from scipy.io import wavfile
import numpy as np
import matplotlib.pyplot as plt
from scipy import fftpack


a = wavfile.read('wooguy3.wav')
stereo_sig = a[1]
# convert it to a mono
mono_sig = stereo_sig[:,0]
# Fast Fourier Transformation of the signal
sig_fft = fftpack.fft(mono_sig)
# Amplitude by taking absolute
amp = np.abs(sig_fft)
# Freqeuncy
freq = fftpack.fftfreq(mono_sig.size, d=0.02)

# Amplitude(loudness) vs. Frequency(pitch)
# plot amplitude (y-axis) against frequency (x-axis)
plt.plot(freq, amp)
plt.title('Original Signal')
plt.xlabel('Frequency')
plt.ylabel('Magnitude')
plt.show()

# create a copy
wooguy = sig_fft.copy()
# create a mast
mask_pos = np.logical_and(freq < 0.7, freq > 0.55)
mask_neg = np.logical_and(freq < -0.55, freq > -0.7)
mask = np.logical_or(mask_pos, mask_neg)
# zeroing the fft ranges corresponding to the wooguy's frequency
wooguy[mask] = 0
# plot amp against freq
amp_wooguy = np.abs(wooguy)
plt.plot(freq, amp_wooguy)
plt.title('Resulted Signal')
plt.xlabel('Frequency')
plt.ylabel('Magnitude')
plt.show()

filtered_sig = fftpack.ifft(wooguy)
amp1 = np.abs(filtered_sig)
int_filtered_sig = filtered_sig.astype(np.int16)


wavfile.write('result.wav',a[0], int_filtered_sig)

