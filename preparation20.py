#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat May 19 14:19:15 2018

@author: yoobin
"""

from scipy import optimize
import numpy as np
import matplotlib.pyplot as plt

def test_func(x, a, b):
    return a * np.sin(b*x)

x_data = np.linspace(-5, 5, num=50)
y_data = 2.9 * np.sin(1.5 * x_data) + np.random.normal(size=50)

popt, pcov = optimize.curve_fit(test_func, x_data, y_data)

plt.plot(x_data, y_data, '-b', label = 'data')

plt.plot(x_data, test_func(x_data, *popt), '-r', label = 'curve_fit')
plt.show()